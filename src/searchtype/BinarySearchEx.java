/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package searchtype;

/**
 *
 * @author Dream Tech
 */


public class BinarySearchEx {
int size=100000;
int [] binarySearchAr;
int searchNumber;

//search element
int low=0;
int high;
int mid;

/*

binary search o(log n)
must be order
*/
    public BinarySearchEx(int searchNumber) {
        this.searchNumber=searchNumber;
        binarySearchAr=new int[size];
        addItem();
        
    }
    
    private void addItem(){
        for(int i=0;i<size;i++){
            binarySearchAr[i]=i+1;
        }
    }
    
    public void searchAlg(){
        high=binarySearchAr.length-1;
        mid=0;
        int numTry=0;
        boolean isFound=false;
        while (!isFound) { 
            numTry++;
            //if search for all tree and not found so low will be grater than high
            if(low>high){
                System.err.println("Number not found");
                System.err.println("number of Try: "+numTry);
                break;
            }
            mid=low+((high-low)/2);
            if(binarySearchAr[mid]==searchNumber){
                System.out.println("Number is Found ");
                 System.err.println("number of Try: "+numTry);
                isFound=true;
            }
            if(binarySearchAr[mid]<searchNumber){
                low=mid+1;
            }
            if(binarySearchAr[mid]>searchNumber){
                high=mid-1;
            }
                   
                  
        }
    }
    
    
    
}
