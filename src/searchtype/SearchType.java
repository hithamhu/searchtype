/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package searchtype;

/**
 *
 * @author Dream Tech
 */
public class SearchType {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
//        //Linear search
//        int size=1000000;
//        int[] numbers=new int[size];
//        
//        //add item to array
//        for(int i=0;i<size;i++){
//            numbers[i]=i+1;
//        }
//        
//        //serach for custom item by linear search o(n)
//        int itemSearch=5555;
//        int numOfTry=0;
//        for(int i=0;i<size;i++){// n time
//            numOfTry++;//c1
//            if(numbers[i]==itemSearch){  //c2
//                System.out.println("item is found");  //c3
//                System.out.println("number of try: "+numOfTry);  //c4
//                break;//to out from loop  c5
//                
//                //big-o for linear search =(c1+c2+c3+c4+c5+n) =o(n) //added
//                
//            }
//            
//        }

       //BINARY SEARCH
       BinarySearchEx binarySearchEx=new BinarySearchEx(1045000);
       
       binarySearchEx.searchAlg();
       
       //interpolation search alg
       InterpolationAlg interpolationAlg=new InterpolationAlg(99905);
       interpolationAlg.searchAlg();
              
    }
    
}
